import { ImageApiResponse } from "../Utils/imageApiUtils";
import { OpenWeatherAPICurrentWeatherResponse } from "../Utils/weatherApiUtils";

export const fakeData: OpenWeatherAPICurrentWeatherResponse = {
  visibility: 16093,
  coord: {
    lat: 5,
    lon: 1,
  },
  wind: {
    speed: 1.5,
    deg: 350,
  },
  weather: [
    {
      id: 800,
      main: "Clear",
      description: "clear sky",
      icon: "01d",
    },
  ],
  main: {
    temp: 282.55,
    feels_like: 281.86,
    temp_min: 280.37,
    temp_max: 284.26,
    pressure: 1023,
    humidity: 100,
  },
};

export const fakeImgData: ImageApiResponse = {
  page: 1,
  photos: [
    {
      id: 1,
      url: "string",
      src: {
        original:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        large2x:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        large:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        medium:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        small:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        portrait:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        landscape:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
        tiny:
          "https://images.pexels.com/photos/290275/pexels-photo-290275.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
      },
    },
  ],
};
