import {
  Box,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import React, { useState } from "react";
import "./App.css";
import { CitySearch } from "./Components/CitySearch";
import { CurrentWeather } from "./Components/CurrentWeather";
import { WeatherForecast } from "./Components/WeatherForecast";
import { defaultLocation } from "./constants";
import { useFetchWeather } from "./hooks/useFetchWeather";
import { useFetchWeatherForecast } from "./hooks/useFetchWeatherForecast";
import { useLocalStorage } from "./hooks/useLocalStorage";

export type DisplayMode = "Current weather" | "Weather forecast";

function App() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [defaultLocationState, setDefaultLocationState] = useLocalStorage(
    "defaultLocation"
  );

  if (!defaultLocationState)
    (setDefaultLocationState as (location: string) => void)(defaultLocation);
  const [city, setCity] = useState<string>(
    defaultLocationState ? (defaultLocationState as string) : defaultLocation
  );

  const [displayMode, setDisplayMode] = useState<DisplayMode>(
    "Current weather"
  );

  const currentWeather = useFetchWeather(city, onOpen);
  const weatherForecast = useFetchWeatherForecast(city, onOpen);

  return (
    <Box>
      <CitySearch
        city={city}
        setCity={setCity}
        displayMode={displayMode}
        setDisplayMode={setDisplayMode}
      />
      {process.env.REACT_APP_TITLE}
      <Box>
        {displayMode === "Current weather" ? (
          <CurrentWeather
            city={city}
            currentWeather={currentWeather}
            defaultLocation={defaultLocationState as string}
            setDefaultLocation={
              setDefaultLocationState as (location: string) => void
            }
          />
        ) : (
          <WeatherForecast city={city} weatherForecast={weatherForecast} />
        )}
      </Box>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>City not found!</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            We are sorry but we don't support this city at the moment.
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
}

export default App;
