import axios from "axios";

interface Coord {
  lon: number;
  lat: number;
}

interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface OpenWeatherAPICurrentWeatherResponse {
  coord: Coord;
  weather: Weather[];
  main: {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  };
  visibility: number;
  wind: {
    speed: number;
    deg: number;
  };
}

export interface OpenWeatherAPIWeatherForecastResponse {
  city: {
    id: number;
    name: string;
    coord: Coord;
  };
  list: {
    clouds: number;
    deg: number;
    speed: number;
    dt: number;
    humidity: number;
    sunrise: number;
    sunset: number;
    feels_like: {
      day: number;
      eve: number;
      morn: number;
      night: number;
    };
    temp: {
      day: number;
      eve: number;
      max: number;
      min: number;
      mor: number;
      night: number;
    };
    weather: Weather[];
  }[];
}

const apiKey = process.env.REACT_APP_WEATHER_API_KEY;

//Returns api url based on city
const getCurrentWeatherApiUrl = (city: string) => {
  return `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`;
};

//Returns api url based on city
const getWeatherForecastApiUrl = (city: string) => {
  return `https://api.openweathermap.org/data/2.5/forecast/daily?q=${city}&cnt=7&units=metric&appid=${apiKey}`;
};

//Fetches current weather from OpenWeatherApi
export const fetchCurrentWeather = async (city: string) => {
  try {
    const weatherResp = await fetch(getCurrentWeatherApiUrl(city));
    if (weatherResp.status === 200)
      return (await weatherResp.json()) as OpenWeatherAPICurrentWeatherResponse;
    if (weatherResp.status === 404) {
      console.error("City not fount");
      throw weatherResp;
    }
    if (weatherResp.status === 401) {
      console.error("There was an error with authorization");
      throw weatherResp;
    }
  } catch (error) {
    return null;
  }
};

//Fetches Weather forecast from OpenWeatherApi
export const fetchWeatherForecast = async (city: string) => {
  const weatherResp = await axios.get<OpenWeatherAPIWeatherForecastResponse>(
    getWeatherForecastApiUrl(city)
  );
  return weatherResp;
};

//Generates image url based on icon name
export const getWeatherImageUrl = (icon: string) => {
  return `http://openweathermap.org/img/wn/${icon}@2x.png`;
};
