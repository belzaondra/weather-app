import axios from "axios";

export interface ImageApiResponse {
  page: number;
  photos: {
    id: number;
    url: string;
    src: {
      original: string;
      large2x: string;
      large: string;
      medium: string;
      small: string;
      portrait: string;
      landscape: string;
      tiny: string;
    };
  }[];
}

const imageApiKey = process.env.REACT_APP_PEXELS_API_KEY;

//Return images url query
const getImageApiUrl = (query: string) => {
  return `https://api.pexels.com/v1/search?query=${query}`;
};

//Return api response with list of images
export const getImages = async (query: string) => {
  const imgResp = await axios.get<ImageApiResponse>(getImageApiUrl(query), {
    headers: {
      Authorization: imageApiKey,
    },
  });

  return imgResp;
};
