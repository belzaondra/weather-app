import { ChakraProvider, ColorModeScript } from "@chakra-ui/react";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Footer } from "./Components/Footer";
import { Navbar } from "./Components/Navbar";
import "./index.css";
import theme from "./theme";

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode="dark" />
      <Navbar />
      <App />
      <Footer />
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
