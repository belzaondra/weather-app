// Time interval in wich data from api are refreshed
export const weatherUpdateInterval = 1000 * 60 * 2; //2 mins
export const defaultLocation = "Vancouver";
