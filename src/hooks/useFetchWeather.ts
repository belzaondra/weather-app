import { useEffect, useState } from "react";
import { weatherUpdateInterval } from "../constants";
import { getImages, ImageApiResponse } from "../Utils/imageApiUtils";
import {
  fetchCurrentWeather,
  OpenWeatherAPICurrentWeatherResponse,
} from "../Utils/weatherApiUtils";

export interface WeatherData {
  weatherResponse?: OpenWeatherAPICurrentWeatherResponse | null;
  imagesResponse?: ImageApiResponse | null;
}

export interface FetchCurrentWeatherState {
  loading: boolean;
  data: WeatherData | null;
}

const handleApis = async (
  city: string,
  state: FetchCurrentWeatherState,
  setState: (data: FetchCurrentWeatherState) => void,
  setLastUpdateDate: (date: Date) => void,
  openNotFoundModalFunction: () => void
) => {
  console.log("Fetching current weather from api.");
  setState({ data: null, loading: true });
  setLastUpdateDate(new Date());

  try {
    const weatherResp = await fetchCurrentWeather(city);
    const imgResp = await getImages(city);
    setState({
      data: {
        weatherResponse: weatherResp,
        imagesResponse: imgResp.data,
      },
      loading: false,
    });
  } catch (error) {
    console.error("An error ocurred during fetching data from api", error);
    if (
      // eslint-disable-next-line eqeqeq
      error?.response?.status == 404 &&
      openNotFoundModalFunction
    )
      openNotFoundModalFunction();
    if (error?.response?.status === 401)
      console.error("There was an error with authorization");
    setState({ loading: false, data: null });
  }
};

//Custom hook for fetching current weather from api
export const useFetchWeather = (
  city: string,
  openNotFoundModalFunction: () => void
) => {
  const [state, setState] = useState<FetchCurrentWeatherState>({
    loading: false,
    data: null,
  });
  const [lastUpdateDate, setLastUpdateDate] = useState(new Date());

  useEffect(() => {
    const weatherUpdater = setInterval(() => {
      //Calculates time difference between two dates
      let timeDifference = Math.abs(
        new Date().getTime() - lastUpdateDate.getTime()
      );

      console.log(
        `Reaming time to update current weather is ${
          (weatherUpdateInterval - timeDifference) / 1000
        }s`
      );

      //If difference is greater than update interval update weather but if last load failed then ignore
      if (weatherUpdateInterval - timeDifference <= 0 && state.data) {
        handleApis(
          city,
          state,
          setState,
          setLastUpdateDate,
          openNotFoundModalFunction
        );
      }
    }, 2000);

    //Clear interval on dismount
    return () => {
      clearInterval(weatherUpdater);
    }; //Hooks for useEffect
  }, [city, lastUpdateDate, openNotFoundModalFunction, state]);

  useEffect(() => {
    handleApis(
      city,
      state,
      setState,
      setLastUpdateDate,
      openNotFoundModalFunction
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [city]);

  return state;
};
