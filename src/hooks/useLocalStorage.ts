import { useState } from "react";

const getValueFromLocalStorage = (key: string) => {
  //returns value from local storage. If value is not found it will return just null value
  return localStorage.getItem(key);
};

const setValueToLocalStorage = (key: string, value: string) => {
  //sets value to local storage inside your browser
  return localStorage.setItem(key, value);
};

export const useLocalStorage = (key: string) => {
  const [stateValue, setStateValue] = useState<string | null>(
    getValueFromLocalStorage(key)
  );

  const updateValue = (value: string) => {
    setValueToLocalStorage(key, value);
    setStateValue(value);
  };

  return [stateValue, updateValue];
};
