import { useEffect, useState } from "react";
import { weatherUpdateInterval } from "../constants";
import {
  fetchWeatherForecast,
  OpenWeatherAPIWeatherForecastResponse,
} from "../Utils/weatherApiUtils";

export interface FetchWeatherForecastState {
  loading: boolean;
  data: OpenWeatherAPIWeatherForecastResponse | null;
}

const handleApi = async (
  city: string,
  state: FetchWeatherForecastState,
  setState: (data: FetchWeatherForecastState) => void,
  setLastUpdate: (date: Date) => void,
  openNotFoundModalFunction: () => void
) => {
  console.log("Fetching weather forecast data from APIs");

  //Updates last date of update
  setLastUpdate(new Date());

  //Clears state
  setState({ loading: true, data: null });

  //Fetches forecast from api
  try {
    const forecast = await fetchWeatherForecast(city);
    setState({ loading: false, data: forecast.data });
  } catch (error) {
    console.error("An error ocurred during fetching data from api", error);
    if (
      // eslint-disable-next-line eqeqeq
      error?.response?.status == "404" &&
      openNotFoundModalFunction
    )
      openNotFoundModalFunction();
    setState({ loading: false, data: null });
  }
};

//Custom hook for fetching weather forecast from api
export const useFetchWeatherForecast = (
  city: string,
  openNotFoundModalFunction: () => void
) => {
  //State which holds current data
  const [state, setState] = useState<FetchWeatherForecastState>({
    loading: false,
    data: null,
  });

  const [lastUpdateDate, setLastUpdateDate] = useState(new Date());

  useEffect(() => {
    const weatherUpdater = setInterval(() => {
      //Calculates difference in ms between two dates
      let timeDifference = Math.abs(
        new Date().getTime() - lastUpdateDate.getTime()
      );

      console.log(
        `Reaming time to update weather forecast is ${
          (weatherUpdateInterval - timeDifference) / 1000
        }s`
      );

      //If time from last update is greater than update interval fetch data from api again; But if last load failed then ignore
      if (weatherUpdateInterval - timeDifference <= 0 && state.data) {
        handleApi(
          city,
          state,
          setState,
          setLastUpdateDate,
          openNotFoundModalFunction
        );
      }
    }, 2000);

    //Clean up function
    return () => {
      clearInterval(weatherUpdater);
    }; //Hooks for use effect
  }, [city, lastUpdateDate, openNotFoundModalFunction, state]);

  useEffect(() => {
    handleApi(
      city,
      state,
      setState,
      setLastUpdateDate,
      openNotFoundModalFunction
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [city]);

  return state;
};
