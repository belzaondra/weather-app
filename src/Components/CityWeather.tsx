import { Box, Heading } from "@chakra-ui/layout";
import {
  Button,
  Flex,
  Icon,
  Image,
  List,
  ListItem,
  Tooltip,
  useToast,
} from "@chakra-ui/react";
import React from "react";
import { RiHomeHeartLine } from "react-icons/ri";
import { WeatherData } from "../hooks/useFetchWeather";
import { getWeatherImageUrl } from "../Utils/weatherApiUtils";

interface CityWeatherProps {
  data: WeatherData;
  city: string;
  defaultLocation: string;
  setDefaultLocation: (location: string) => void;
}

export const CityWeather: React.FC<CityWeatherProps> = ({
  data,
  city,
  defaultLocation,
  setDefaultLocation,
}) => {
  const { imagesResponse, weatherResponse } = data;
  const toast = useToast();

  return (
    <Flex direction="column">
      <Box maxW="sm" borderWidth="1px" borderRadius="lg" overflow="hidden">
        <Image
          src={imagesResponse?.photos[0]?.src?.large}
          fallbackSrc="https://images.pexels.com/photos/4412934/pexels-photo-4412934.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
          alt="City image"
        />

        <Box p="6">
          <Flex justify="center" align="center">
            <Heading as="h3" display="inline" ml={2}>
              {city}
            </Heading>

            {defaultLocation !== city && (
              <Tooltip hasArrow label="Set as default location" placement="top">
                <Button
                  p={0}
                  ml={2}
                  onClick={() => {
                    setDefaultLocation(city);
                    toast({
                      title: "Location changed",
                      description: "Your default location was changed",
                      status: "success",
                      isClosable: true,
                      duration: 3000,
                      position: "bottom-right",
                    });
                  }}
                >
                  <Icon as={RiHomeHeartLine} w={6} h={6} />
                </Button>
              </Tooltip>
            )}
          </Flex>

          <Flex mt={2}>
            <List spacing={2} w="100%">
              <ListItem>
                <Flex justify="center">
                  <Image
                    src={getWeatherImageUrl(
                      weatherResponse?.weather[0]?.icon || ""
                    )}
                  />
                </Flex>
              </ListItem>
              <ListItem>
                <Flex justify="center">Weather</Flex>
              </ListItem>
              <ListItem>
                <Flex>
                  <Box w="50%" textAlign="right" pr={2}>
                    Temperature:
                  </Box>
                  <Box w="50%" textAlign="left" pl={2}>
                    {weatherResponse?.main.temp} °C
                  </Box>
                </Flex>
              </ListItem>
              <ListItem>
                <Flex>
                  <Box w="50%" textAlign="right" pr={2}>
                    Feels like:
                  </Box>
                  <Box w="50%" textAlign="left" pl={2}>
                    {weatherResponse?.main.feels_like} °C
                  </Box>
                </Flex>
              </ListItem>
              <ListItem>
                <Flex justify="center">Wind</Flex>
              </ListItem>
              <ListItem>
                <Flex>
                  <Box w="50%" textAlign="right" pr={2}>
                    Speed:
                  </Box>
                  <Box w="50%" textAlign="left" pl={2}>
                    {weatherResponse?.wind.speed} m/s
                  </Box>
                </Flex>
              </ListItem>

              <ListItem>
                <Flex>
                  <Box w="50%" textAlign="right" pr={2}>
                    Degrees:
                  </Box>
                  <Box w="50%" textAlign="left" pl={2}>
                    {weatherResponse?.wind.deg}°
                  </Box>
                </Flex>
              </ListItem>
            </List>
          </Flex>
        </Box>
      </Box>
    </Flex>
  );
};
