import { Box, Flex, Icon, Spinner, Text } from "@chakra-ui/react";
import React from "react";
import { FcCancel } from "react-icons/fc";
import { FetchWeatherForecastState } from "../hooks/useFetchWeatherForecast";
import { WeatherForecastTable } from "./WeatherForecastTable";

interface WeatherForecastProps {
  city: string;
  weatherForecast: FetchWeatherForecastState;
}

export const WeatherForecast: React.FC<WeatherForecastProps> = ({
  city,
  weatherForecast,
}) => {
  return (
    <>
      {/* If loading */}
      {weatherForecast.loading ? (
        <Flex justify="center" align="center" direction="column" m={4}>
          <Text>Loading weather forecast</Text>
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      ) : // If no data
      !weatherForecast.data ? (
        <Flex justify="center">
          <Box>
            <Text>
              Data are not available at the moment <Icon as={FcCancel} />
            </Text>
          </Box>
        </Flex>
      ) : (
        <>
          {/* If data */}
          <Flex justify="center" mt={4}>
            <WeatherForecastTable data={weatherForecast.data} city={city} />
          </Flex>
        </>
      )}
    </>
  );
};
