import { SearchIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  HStack,
  Icon,
  useColorModeValue,
  useRadioGroup,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import React from "react";
import { InputField } from "./InputField";
import { RadioCard } from "./RadioCard";
import { GoLocation } from "react-icons/go";

interface CitySearchProps {
  city: string;
  setCity: (city: string) => void;
  displayMode: string;
  setDisplayMode: (mode: "Current weather" | "Weather forecast") => void;
}

export const CitySearch: React.FC<CitySearchProps> = ({
  city,
  setCity,
  displayMode,
  setDisplayMode,
}) => {
  //Custom colors for dark and light theme
  const backgroundColor = useColorModeValue("white", "#333333");
  const buttonHoverColor = useColorModeValue("#E2E8F0", "#444444");

  const options = ["Current weather", "Weather forecast"];

  const { getRootProps, getRadioProps } = useRadioGroup({
    name: "framework",
    defaultValue: displayMode,
    onChange: (v) =>
      setDisplayMode(v as "Current weather" | "Weather forecast"),
  });

  const group = getRootProps();

  return (
    <Box
      backgroundImage="url(https://images.pexels.com/photos/6272360/pexels-photo-6272360.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260)"
      w="400"
      h="400"
    >
      <Flex justify="center" align="center" h="100%" direction="column">
        <Formik
          initialValues={{ city: city }}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              setCity(values.city);
              setSubmitting(false);
            }, 400);
          }}
        >
          {({ values, handleChange, isSubmitting }) => (
            <Box w="80%">
              <Form>
                <InputField
                  name="city"
                  placeholder="city"
                  backgroundColor={backgroundColor}
                  rightInputElement={<Icon as={GoLocation} />}
                />

                <Flex>
                  <Button
                    m="auto"
                    mt={4}
                    type="submit"
                    backgroundColor={backgroundColor}
                    _hover={{
                      backgroundColor: buttonHoverColor,
                    }}
                    isLoading={isSubmitting}
                  >
                    Find <SearchIcon ml={2} />
                  </Button>
                </Flex>
              </Form>
            </Box>
          )}
        </Formik>
        <HStack {...group} mt={8}>
          {options.map((value) => {
            const radio = getRadioProps({ value });
            return (
              <RadioCard
                key={value}
                props={radio}
                buttonStyle={{
                  backgroundColor: backgroundColor,
                  _hover: { backgroundColor: buttonHoverColor },
                }}
              >
                {value}
              </RadioCard>
            );
          })}
        </HStack>
      </Flex>
    </Box>
  );
};
