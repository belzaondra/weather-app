import {
  Flex,
  Heading,
  Image,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import React from "react";
import {
  getWeatherImageUrl,
  OpenWeatherAPIWeatherForecastResponse,
} from "../Utils/weatherApiUtils";

interface WeatherForecastTableProps {
  data: OpenWeatherAPIWeatherForecastResponse | null;
  city: string;
}

export const WeatherForecastTable: React.FC<WeatherForecastTableProps> = ({
  data,
  city,
}) => {
  var d = new Date();
  //Array with day in week
  var weekday: string[] = new Array(7);
  weekday[0] = "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

  var today = d.getDay();
  return (
    <Flex w="100%" direction="column" align="center">
      <Heading textAlign="center">{city}</Heading>

      <Table variant="simple" w="80%" mt={4}>
        <Thead>
          <Tr>
            <Th>Day</Th>
            <Th>Min temp</Th>
            <Th>Max temp</Th>
            <Th>Day temp</Th>
            <Th>Night temp</Th>
            <Th>Humidity</Th>
            <Th>Sky</Th>
            <Th>Wind</Th>
          </Tr>
        </Thead>
        <Tbody>
          {data?.list.map((day, index) => {
            return (
              <Tr key={day.dt}>
                <Td>{weekday[(today + index) % 7]}</Td>
                <Td isNumeric>{day.temp.min} °C</Td>
                <Td isNumeric>{day.temp.max} °C</Td>
                <Td isNumeric>{day.temp.day} °C</Td>
                <Td isNumeric>{day.temp.night} °C</Td>
                <Td isNumeric>{day.humidity} %</Td>
                <Td>
                  <Image
                    h="50"
                    w="auto"
                    src={getWeatherImageUrl(day.weather[0].icon)}
                  />
                </Td>
                <Td>
                  {day.deg}° at {day.speed} m/s
                </Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </Flex>
  );
};
