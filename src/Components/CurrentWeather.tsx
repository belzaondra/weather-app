import { Icon } from "@chakra-ui/icons";
import { Flex, Spinner, Box, Text } from "@chakra-ui/react";
import React from "react";
import { FcCancel } from "react-icons/fc";
import { FetchCurrentWeatherState } from "../hooks/useFetchWeather";
import { CityWeather } from "./CityWeather";

interface CurrentWeatherProps {
  currentWeather: FetchCurrentWeatherState;
  city: string;
  defaultLocation: string;
  setDefaultLocation: (location: string) => void;
}

export const CurrentWeather: React.FC<CurrentWeatherProps> = ({
  currentWeather,
  city,
  defaultLocation,
  setDefaultLocation,
}) => {
  return (
    <>
      {/* If loading */}
      {currentWeather.loading ? (
        <Flex justify="center" align="center" direction="column" m={4}>
          <Text>Loading current weather</Text>
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      ) : // If no data
      !currentWeather.data ? (
        <Flex justify="center">
          <Box>
            <Text>
              Data are not available at the moment <Icon as={FcCancel} />
            </Text>
          </Box>
        </Flex>
      ) : (
        <>
          {/* If data */}
          <Flex justify="center" mt={4}>
            <CityWeather
              data={currentWeather.data}
              city={city}
              defaultLocation={defaultLocation}
              setDefaultLocation={setDefaultLocation}
            />
          </Flex>
        </>
      )}
    </>
  );
};
