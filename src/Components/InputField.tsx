import { Input } from "@chakra-ui/input";
import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useField } from "formik";
import React, { ReactNode } from "react";

type InputFieldProps = React.InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  label?: string;
  backgroundColor?: string;
  rightInputElement?: ReactNode;
};

export const InputField: React.FC<InputFieldProps> = ({
  label,
  backgroundColor,
  size: _,
  rightInputElement,
  ...props
}) => {
  const [field, { error }] = useField(props);
  return (
    <FormControl isInvalid={!!error}>
      {label ? <FormLabel htmlFor={field.name}>{label}</FormLabel> : null}
      <InputGroup>
        <Input
          {...field}
          {...props}
          id={field.name}
          placeholder={props.placeholder}
          backgroundColor={backgroundColor ? backgroundColor : ""}
        />
        {props.children}
        {rightInputElement && (
          <InputRightElement children={rightInputElement} />
        )}
      </InputGroup>

      {error ? <FormErrorMessage>{error}</FormErrorMessage> : null}
    </FormControl>
  );
};
