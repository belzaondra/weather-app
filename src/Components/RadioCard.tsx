import { Box, ChakraProps, useRadio, UseRadioProps } from "@chakra-ui/react";
import React, { ReactNode } from "react";

type RadioCardProps = ChakraProps & {
  props: UseRadioProps;
  children: ReactNode;
  buttonStyle: ChakraProps;
};

export const RadioCard: React.FC<RadioCardProps> = ({
  props,
  children,
  buttonStyle,
  ...rest
}) => {
  const { getInputProps, getCheckboxProps } = useRadio(props);

  const input = getInputProps();
  const checkbox = getCheckboxProps();
  return (
    <Box as="label" {...rest}>
      <input {...input} />
      <Box
        {...checkbox}
        {...buttonStyle}
        cursor="pointer"
        borderWidth="1px"
        borderRadius="md"
        boxShadow="md"
        _checked={{
          borderColor: "telegram.600",
          borderWidth: "3px",
        }}
        _focus={{
          boxShadow: "outline",
        }}
        px={2}
        py={2}
      >
        {children}
      </Box>
    </Box>
  );
};
